

console.log(document.querySelector("#txt-first-name"));

console.log(document)


/*
Alternative:

document.getElementById("txt-first-name");
document.getElementsByClassName()
document.getElementsByTagName()


*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

//Event Listeners

/*
 	selectedElement.addEventListener('event', function);
*/


function printFullName(event) {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;

}

txtFirstName.addEventListener('keyup', printFullName);
txtLastName.addEventListener('keyup', printFullName);




